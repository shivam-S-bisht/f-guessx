import React from 'react';
import {View, Text, TextInput, TouchableOpacity,  Alert, AsyncStorage} from 'react-native';
import axios from 'react-native-axios/lib/axios';



export default class GuessTheWord extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            userid : null,
            encrword : null,
            wordid : null,
            gameid : null,
            securityid : null,

            guessLetter : null,
            first : 1,
            reset : null,
            chancesLeft : null,
            win:0,

            data : {}
        }
    }




    async componentDidMount(){
        var propscategory = (this.props.navigation.state.params.category).toString();
        console.log(propscategory)
        await AsyncStorage.getItem("userdata", async (err, res)=>{

            const data = JSON.parse(res);
            console.log(data)
            

            this.setState({securityid : data.securityid, userid : data.userid, userprint : data.userprint, data : data});
    
        

        
            await axios.post('http://127.0.0.1:8000/play/userauth/firstcall/', 
                {
                    "data" : {
                        "userid" : data.userid,
                        "category" : this.props.navigation.state.params.category,
                        "userprint" : data.userprint
                
                    },
                
                    "securityid" : data.securityid
                }
            )
            .then(async res=>{
                
                if(res.data.status){
                    const statedata = this.state.data;
                    statedata.securityid = res.data.securityid;
                    statedata.nogplayed += 1;

                    this.setState({
                        encrword : res.data.data.encrypted_word, 
                        wordid : res.data.data.word_id, 
                        gameid : res.data.data.game_id,
                        securityid : res.data.securityid,
                        chancesLeft : res.data.data.encrypted_word.length,
                        data : statedata
                    });

                    await AsyncStorage.setItem("userdata", JSON.stringify(this.state.data));
                

                }
                else{
                    console.log(res.data.error)
                }
            })
            .catch(err=>{
                console.log(err)
            })

         });
    }




      async isCorrect(){
          console.log(this.state)
          
        await axios.post('http://127.0.0.1:8000/play/userauth/wordguess/', 
        {
            "data" : {
                "userid" : this.state.userid,
                "category" : this.props.navigation.state.params.category,
                "userprint" : this.state.userprint,
                "gameid" : this.state.gameid,
                "guessed" : this.state.guessLetter,
                "wordid" : this.state.wordid,
                "encryptedword" : this.state.encrword
            },
        
            "securityid" : this.state.securityid
            
        })
        .then(async res=>{
            



            if(res.data.status){
                const statedata = this.state.data;
                statedata.securityid = res.data.securityid;

                this.setState({securityid : res.data.securityid, encrword : res.data.data.encryptedword, chancesLeft : this.state.chancesLeft - 1, win : res.data.data.win});
                
                

                if (!this.state.chancesLeft && !res.data.data.win){
                    Alert.alert('no chances left, you lost');
                    setTimeout(()=>{
                        this.props.navigation.pop();
                    }, 2000)
                    
                }
                else if (!this.state.chancesLeft && res.data.data.win){
                    
                    statedata.nogwin += 1
                    this.setState({data : statedata})


                    Alert.alert('you guessed it correct');
                    setTimeout(()=>{
                        this.props.navigation.pop();
                    }, 2000)
                }
                else if (res.data.data.win){
                    statedata.nogwin += 1
                    this.setState({data : statedata})
                    
                    
                    Alert.alert('you guessed it correct');
                    setTimeout(()=>{
                        this.props.navigation.pop();
                    }, 2000)
                }
                this.setState({data : statedata});
                await AsyncStorage.setItem("userdata", JSON.stringify(this.state.data))

                
                
            }
            else{
                console.log(res.data.error)
            }
        })
        .catch(err=>{
            console.log(err)
        })
    }




    render(){
        return(
            <View style = {{flex : 1, top : 30, alignItems : 'center', justifyContent : 'center'}}>

                <Text style = {{}}>{this.state.encrword}</Text>
                <TextInput 
                    value = {this.props.reset}
                    style = {{}}
                    placeholder = 'guess the letter'
                    onChangeText = {guessLetter=>{
                        this.setState({guessLetter : guessLetter})
                    }}
                    maxLength = {1}
                    ref = {inp => this.textinput = inp}
                />
                <TouchableOpacity
                    style = {{top : 10}}
                    onPress = {()=>{
                        this.isCorrect();
                        this.textinput.clear();
                        
                    }}
                >
                    <Text>Guess</Text>
                </TouchableOpacity>

            </View>
        );
    }
}