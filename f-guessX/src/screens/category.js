import React from 'react';
import { View, Text, Picker } from 'react-native';
import axios from 'react-native-axios';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Category extends React.Component{

    

    constructor(props){
        super(props);

        this.state = {
            data : null,
            counter : 0,
            whichCate : null
        }
        
    }




    async fun(){
        await axios.get('http://127.0.0.1:8000/admin/categorydetails/')
        .then(res=>{
            
            this.setState({data : res.data.data, counter : 1})
            
        })
        .catch(err=>{console.log(err)})
        
        this.setState({pass : this.state.data[0]['name']})

        
        return <Text>some</Text>
    }
    



    
 
    render(){
        
        if(this.state.counter == 0) {
            return (
                <View style = {{flex : 1}}>

                <TouchableOpacity
                    style = {{alignItems : 'center',justifyContent : 'center', marginTop : 30}}
                    onPress = {()=>{return this.fun()}}
                >
                <Text>Choose Category</Text>
                </TouchableOpacity>
                
           

                
            </View>
            );
        }

        else {
            return (
            <View style = {{flex : 1}}>

                {this.state.data.map((elm, key)=>{
                    return <View
                            style = {{ alignItems : 'center', justifyContent : 'center', flex : 1}}
    
                            >
                                <TouchableOpacity 
                                    key = {key}
                                    onPress = {()=>{
                                        
                                        this.props.navigation.replace('guessword', {category :  elm['name']})
                
                                    }}
                                >
                                <Text>{elm['name']}</Text>
                                
                                </TouchableOpacity>
                            </View>
                    }
                    )}
                
            </View>
            )
        }

        
    }
}