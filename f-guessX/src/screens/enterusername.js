import React from 'react';
import {View, Text, TouchableOpacity, TextInput, Alert, AsyncStorage} from 'react-native';
import axios from 'react-native-axios/lib/axios';
import sha256 from 'js-sha256';


export default class EnterUername extends React.Component{


    constructor(props){
        super(props);
        this.state = {
            
            username :  '',
            userid : null,
            securityid :null,
            profilepic : null,
            nogplayed : null,
            nogwin : null,
            userprint : null,
            service : null
            
        }
        this.api = null
    }


    componentDidMount(){
        console.log(this.props)
        
        if(this.props.navigation.state.params.login != 'ExistingUser'){
            this.api = 'http://127.0.0.1:8000/play/usercreate/usersignupin/';
            
        }else{
            this.api = 'http://127.0.0.1:8000/play/userauth/';
            this.sendCred();
        }
        

    }
    




    createUUID(){
        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (dt + Math.random()*16)%16 | 0;
            dt = Math.floor(dt/16);
            return (c=='x' ? r :(r&0x3|0x8)).toString(16);
        });
        return uuid;
    }



    async sendCred(){
        const userid =this.createUUID();
        const userprint = sha256(Math.floor(Math.random()*1000000).toString())
        this.setState({userid : userid, userprint : userprint});
        

        await axios.post(this.api, 
        {
            "data" : {
                "userid" : userid,
                "username" : this.state.username, 
                "profilepic" : "3",
                "userprint" : userprint,
                "service" : this.props.navigation.state.params.service,
                "fbid" : this.props.navigation.state.params.fbid,
                "phone" : this.props.navigation.state.params.phone
            },
        
            "securityid" : 'tgh'
            
        }
        )
        .then(async res=>{
            if(!res.data.status){
                Alert.alert(res.data.error);
                setTimeout(()=>{
                    this.props.navigation.replace('loginsignup')
                }, 2000)
            }
            else if(res.data.status){
                console.log(res.data)
                this.setState({
                    userid : res.data.data.userid,
                    username : res.data.data.username,
                    securityid : res.data.securityid,
                    profilepic : res.data.data.profilepic,
                    nogplayed : res.data.data.n_o_g,
                    nogwin : res.data.data.n_o_g_wins,
                    service : this.props.navigation.state.params.service
                })
                console.log(this.state)

                await AsyncStorage.setItem(
                    "userdata",
                    JSON.stringify(this.state)
                );

                
                setTimeout(()=>{
                    this.props.navigation.replace('userdetails')
                }, 2000)
            }
            
            

            // console.log(res.data)
        })
        .catch(err=>{
            // Alert.alert('Alresy exi')
            console.log(err)
        })
        
    }

    

    render(){
        return(
            <View style = {{flex : 1, top : 30, alignItems :'center', justifyContent : 'center'}}>
                <TextInput 
                    placeholder = 'Enter username'
                    onChangeText = {(username)=>{
                        this.setState({username : username})
                        
                    }}
                    maxLength = {20} 
                />
                <TouchableOpacity
                    onPress = {()=>{this.sendCred()}}
                >
                    <Text>Submit</Text>
                </TouchableOpacity>
            </View>
        );
    }
}