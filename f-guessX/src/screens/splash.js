// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Image, ProgressBarAndroid, AsyncStorage } from 'react-native';


export default class Splash extends React.Component{


    constructor(props){
        super(props);
        // console.log(props)
    }


    async componentDidMount(){
        
         
        
          const userdata = await AsyncStorage.getItem("userdata");

          if(userdata != null){
            console.log(userdata)
          setTimeout(()=>{this.props.navigation.replace('userdetails')}, 2000)

          }
          else{
            setTimeout(()=>{this.props.navigation.replace('loginsignup')}, 2000)
            console.log(userdata)
          }
          
    }



    render(){


        return (
            <View style={{flex: 1, backgroundColor: 'rgb(22, 14, 37)'}}>
              <View style={{top: "30%", alignSelf:"center"}}>
                <ProgressBarAndroid 
                    styleAttr = 'LargeInverse'
                    indeterminate = {true}
                />
                
              </View>
            </View>
          );

    }
}



