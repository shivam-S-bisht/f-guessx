import * as React from "react";
import { Text, View, TextInput, Button, StyleSheet, TouchableOpacity, Platform, Alert } from "react-native";
import { FirebaseRecaptchaVerifierModal } from "expo-firebase-recaptcha";
import * as firebase from "firebase";



try {
  firebase.initializeApp({
    apiKey: "AIzaSyBwFIHWQ1q91cKwkpKw1YisvvFHF4fZA8M",
    authDomain: "guessex-61100.firebaseapp.com",
    databaseURL: "https://guessex-61100.firebaseio.com",
    projectId: "guessex-61100",
    storageBucket: "guessex-61100.appspot.com",
    messagingSenderId: "1079863234933",
    appId: "1:1079863234933:web:154ca137ea67fba9aa433c",
    measurementId: "G-8KK8N2R8C7"

  });
} catch (err) {
  
}   

export default class PhoneRedirect extends React.Component{


    constructor(props){
        super(props);

        this.recaptchaVerifier = null;
        this.firebaseConfig = firebase.apps.length ? firebase.app().options : undefined;


        this.state = {
            fbid : null, 
            phoneNumber : null,
            verificationId : null,
            verificationCode : null,
            message : ((!this.firebaseConfig || Platform.OS === 'web')
            ? { text: "To get started, provide a valid firebase config in App.js and open this snack on an iOS or Android device."}
            : undefined)
        }
   
    }


    

    componentDidMount(){
      console.log(this.props)
    }


    

    render(){
        

        return (
          <View style={{ padding: 20, marginTop: 50 }}>
            <FirebaseRecaptchaVerifierModal
            
              ref={recaptchaVerifier => this.recaptchaVerifier = recaptchaVerifier}
              firebaseConfig={this.firebaseConfig}
            />
            <Text style={{ marginTop: 20 }}>Enter phone number</Text>
            <TextInput
              style={{ marginVertical: 10, fontSize: 17 }}
              placeholder="+1 999 999 9999"
              autoFocus
              autoCompleteType="tel"
              keyboardType="phone-pad"
              textContentType="telephoneNumber"
              onChangeText={(phoneNumber) => this.setState({phoneNumber : phoneNumber})}
            />
            <Button
              title="Send Verification Code"
              disabled={!this.state.phoneNumber}
              onPress={async () => {
                
                try {
                  const phoneProvider = new firebase.auth.PhoneAuthProvider();
                  const verificationId = await phoneProvider.verifyPhoneNumber(
                    this.state.phoneNumber,
                    this.recaptchaVerifier
                  );
                  this.setState({verificationId : verificationId})
                  Alert.alert('OTP sent tp your phone')
                  
                } catch (err) {
                    Alert.alert('something went wrong')
                }
              }}
            />
            <Text style={{ marginTop: 20 }}>Enter Verification code</Text>
            <TextInput
              style={{ marginVertical: 10, fontSize: 17 }}
              editable={!!this.state.verificationId}
              placeholder="123456"
              onChangeText={verificationCode => this.setState({verificationCode : verificationCode})}
            />
            <Button
              title="Confirm Verification Code"
              disabled={!this.state.verificationId}
              onPress={async () => {
                try {
                  const credential = firebase.auth.PhoneAuthProvider.credential(
                    this.state.verificationId,
                    this.state.verificationCode
                  );
                  await firebase.auth().signInWithCredential(credential);
                  Alert.alert('login success')
                  setTimeout(()=>{this.props.navigation.replace('enterusername', {service : 'Phone', login : this.props.navigation.state.params.login, phone : this.state.phoneNumber, fbid : this.state.fbid})})
                  
                } catch (err) {
                    Alert.alert('invalid otp')
                  
                }
              }}
            />
            
          </View>
  );
}

}
