import React, { Component } from 'react';
import {View, BackHandler} from 'react-native';
import SkipOrNot from '../components/skipornot';




export default class LoginSignup extends React.Component{

    constructor(props){
        super(props)
    }

    componentDidMount(){
        // BackHandler.addEventListener('hardwareBackPress', this.handlebackbutton);
        // BackHandler.removeEventListener('hardwareBackPress', this.handlebackbutton);
       
        
    }


    handlebackbutton(){
        
        return true
    }


    render(){

        

        return(
            
                <View style = {{flex : 1}}>  
                    <SkipOrNot 
                        elm = 'ExistingUser'
                        props = {this.props}

                    />

                    <SkipOrNot 
                        elm = 'NewUser'
                        props = {this.props}

                    />

                    <SkipOrNot 
                        elm = 'Skip'
                        props = {this.props}

                    />
                </View>
            
        );
    }
}