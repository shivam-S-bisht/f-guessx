import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

export default class SkipOrNot extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            phone : null,
            fbid : null
        }
    }

    render(){

        if(this.props.elm != 'Skip'){
            return(
                <View style = {{flex : 1, alignItems : 'center', justifyContent : "center"}}>
                    <TouchableOpacity
                        onPress = {()=>{this.props.props.navigation.replace('login', {login : this.props.elm})}}
                    >
                        <Text>{this.props.elm}</Text>
                    </TouchableOpacity>
                </View>
            );
        }

        else{
            return(
                <View style = {{flex : 1, alignItems : 'center', justifyContent : "center"}}>
                    <TouchableOpacity
                        onPress = {()=>{this.props.props.navigation.replace('enterusername', {login : this.props.elm, service : this.props.elm, fbid : this.state.fbid, phone : this.state.phone})}}
                    >
                        <Text>{this.props.elm}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        
    }
}