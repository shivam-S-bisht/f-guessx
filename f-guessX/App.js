import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

// <---------screens-------------->
import Splash from './src/screens/splash';
import Login from './src/screens/login';
import UserDetails from './src/screens/userdetails';
import Category from './src/screens/category';
import LoginSignup from './src/screens/loginsignup';
import PhoneRedirect from './src/screens/phoneredirect';
import GuessTheWord from './src/screens/guessword';
import EnterUername from './src/screens/enterusername';









const stackContainer = createStackNavigator({
  splash : {
    screen : Splash,
    navigationOptions : {
      headerShown : false
    }
  },

  loginsignup : {
    screen : LoginSignup,
    navigationOptions : {
      headerShown : false
    }
  },

  enterusername : {
    screen : EnterUername, 
    navigationOptions : {
      headerShown : false
    }
  },

  login : {
    screen : Login, 
    navigationOptions : {
      headerShown : false,
      
    }
  },

  phoneredirect:{
    screen : PhoneRedirect,
    navigationOptions : {
      headerShown : false
    }
  },

  userdetails : {
    screen : UserDetails,
    navigationOptions : {
      headerShown : false
    }
  },

  category : {
    screen : Category,
    navigationOptions : {
      headerShown : false
    }
  },
  
  guessword : {
    screen : GuessTheWord,
    navigationOptions : {
      headerShown : false
    }
  }


})


export default createAppContainer(stackContainer)